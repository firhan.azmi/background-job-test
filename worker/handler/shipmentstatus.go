package handler

import (
	"context"
	"fmt"
	"time"
)

func ShipmentStatus(ctx context.Context, args ...interface{}) error {
	fmt.Printf("%d - shipment status job working", time.Now().Unix())

	return nil
}
