package handler

import (
	"time"

	"github.com/aptible/supercronic/cronexpr"
	"gitlab.com/firhan.azmi/background-job-test/client/client"
	"gitlab.com/firhan.azmi/background-job-test/client/constants"
)

func ScheduleNextJob(name string, expression string) error {
	c, err := client.GetClient()
	if err != nil {
		return err
	}

	nextExecution := cronexpr.MustParse(expression).Next(time.Now().UTC())
	job := client.NewScheduledJob(nextExecution, constants.SHIPMENT_STATUS, nil)

	if err := c.Push(job); err != nil {
		return err
	}

	return nil
}
