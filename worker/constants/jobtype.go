package constants

const (
	SHIPMENT_STATUS = "SHIPMENT_STATUS"
)

const (
	SHIPMENT_STATUS_CRON_EXPRESSION = "0 0 * * * * *"
)
