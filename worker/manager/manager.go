package manager

import (
	"context"

	faktoryworker "github.com/contribsys/faktory_worker_go"
	"gitlab.com/firhan.azmi/background-job-test/client/constants"
	"gitlab.com/firhan.azmi/background-job-test/client/handler"
)

func Init() {
	mgr := faktoryworker.NewManager()

	RegisterCronjob(mgr, constants.SHIPMENT_STATUS, constants.SHIPMENT_STATUS_CRON_EXPRESSION, handler.ShipmentStatus)

	if err := mgr.Run(); err != nil {
		panic(err)
	}
}

func RegisterCronjob(manager *faktoryworker.Manager, name string, expression string, fn faktoryworker.Perform) {
	handler := func(ctx context.Context, args ...interface{}) error {
		if err := handler.ScheduleNextJob(name, expression); err != nil {
			return err
		}

		if err := fn(ctx, args); err != nil {
			return err
		}

		return nil
	}

	manager.Register(name, handler)
}
