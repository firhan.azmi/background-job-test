package client

import (
	"sync"

	faktoryclient "github.com/contribsys/faktory/client"
)

var client *faktoryclient.Client
var once sync.Once

func GetClient() (*faktoryclient.Client, error) {
	once.Do(func() {
		var err error
		client, err = faktoryclient.Open()
		if err != nil {
			panic(err)
		}
	})

	return client, nil
}
