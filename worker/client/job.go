package client

import (
	"time"

	faktoryclient "github.com/contribsys/faktory/client"
)

func NewScheduledJob(when time.Time, jobtype string, args ...interface{}) *faktoryclient.Job {
	return &faktoryclient.Job{
		Type:      jobtype,
		Queue:     "default",
		Args:      args,
		Jid:       faktoryclient.RandomJid(),
		At:        when.Format(time.RFC3339Nano),
		CreatedAt: time.Now().UTC().Format(time.RFC3339Nano),
		Retry:     &faktoryclient.RetryPolicyDefault,
	}
}
