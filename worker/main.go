package main

import (
	faktoryclient "github.com/contribsys/faktory/client"
	"gitlab.com/firhan.azmi/background-job-test/client/client"
	"gitlab.com/firhan.azmi/background-job-test/client/constants"
	"gitlab.com/firhan.azmi/background-job-test/client/manager"
)

func main() {
	triggerFirstJob()
	manager.Init()
}

func triggerFirstJob() {
	client, err := client.GetClient()
	if err != nil {
		panic(err)
	}

	if err := client.Push(faktoryclient.NewJob(constants.SHIPMENT_STATUS, nil)); err != nil {
		panic(err)
	}
}
