module gitlab.com/firhan.azmi/background-job-test/client

go 1.18

require (
	github.com/contribsys/faktory v1.6.1
	github.com/contribsys/faktory_worker_go v1.6.0
)

require github.com/aptible/supercronic v0.1.13
